<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dic_models', function (Blueprint $table) {
            $table->longText("name_fa")->index()->change();
            $table->longText("synonym")->comment("مترادف")->nullable()->change();
            $table->longText("Sentence")->comment("جمله انگلیسی")->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dic_models', function (Blueprint $table) {
            $table->string("name_fa")->index()->change();
            $table->string("synonym")->comment("مترادف")->nullable()->change();
            $table->string("Sentence")->comment("جمله انگلیسی")->nullable()->change();
        });
    }
}
