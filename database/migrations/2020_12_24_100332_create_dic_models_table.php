<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDicModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dic_models', function (Blueprint $table) {
            $table->id();
            $table->string("name")->index();
            $table->string("name_fa")->index();
            $table->string("synonym")->comment("مترادف")->nullable();
            $table->string("Sentence")->comment("جمله انگلیسی")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dic_models');
    }
}
