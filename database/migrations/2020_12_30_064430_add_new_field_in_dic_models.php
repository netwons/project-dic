<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldInDicModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dic_models', function (Blueprint $table) {
            $table->json("pronunciation")->comment("تلفظ")->nullable();
            $table->json("path_pronunciation")->comment("مسیر تلفظ")->nullable();
            $table->longText("meaning")->comment("معنی")->nullable();
            $table->json("sentence_similar")->comment("جمله مشابه")->nullable();
            $table->json("sentence_similar2")->comment("جمله مشابه۲")->nullable();
            $table->longText("specialty")->comment("تخصصی")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dic_models', function (Blueprint $table) {
            $table->dropColumn('pronunciation');
            $table->dropColumn('path_pronunciation');
            $table->dropColumn('meaning');
            $table->dropColumn('sentence_similar');
            $table->dropColumn('sentence_similar2');
            $table->dropColumn('specialty');
        });
    }
}
