<?php

namespace Tests\Feature;

use App\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    //public function testBasicTest()
  //  {
//        $this->get('/t')->assertStatus(200);

    //}
    public function testJson()
    {
        $post1=factory(Post::class)->create(
            ['video_url'=>'https://www.youtube.com/jjj']);
        $re=$this->getJson('/t?hasvideo=1');
        $server_post=json_decode($re->getContent());

        $this->assertTrue($this->count($server_post)==1);
    }
}
