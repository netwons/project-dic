<?php

namespace Tests\Unit;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
class PostStoreTest extends TestCase
{
    use RefreshDatabase;
    public function testExample()
    {
        $post=factory(Post::class)->create();
        $dbpost=Post::first();

        $this->assertNotNull($dbpost);
        $this->assertTrue($dbpost->id==$post->id);
    }
    public function testExamplep()
    {
        $post=factory(Post::class)->create([
            'published_at'=>now()->subDay()
        ]);
        $dbpost=Post::Published()->first();
        $this->assertNotNull($dbpost);
        $this->assertTrue($dbpost->id==$post->id);
    }

    public function testnullpublished()
    {
        $post=factory(Post::class)->create([
            'published_at'=>null
        ]);
        $dbpost=Post::Published()->first();
//dump($post->toArray());
//dd($dbpost);
        $this->assertNull($dbpost);
    }
    public function testnullpublishedYet()
    {
        $post=factory(Post::class)->create([
            'published_at'=>now()->addDays(3)
        ]);
        $dbpost=Post::Published()->first();
        $this->assertNull($dbpost);
    }
    //------------------------------------model scopeQ----------------------------------------
    public function testsimpleQ()
    {
        $post=factory(Post::class)->create([
            'title'=>'masoud-fathi',
            'slug'=>''

        ]);
        $dbpost=Post::Q('fathi')->first();
        $this->assertTrue($dbpost->id==$post->id);
    }
    public function testsimpleNotfound()
    {
        $post=factory(Post::class)->create([
            'title'=>'masoud-ali',
            'slug'=>''
        ]);
        $dbpost=Post::Q('fathi')->first();
        $this->assertNull($dbpost);
    }
    public function testsimplebigword()
    {
        $post=factory(Post::class)->create([
            'title'=>'masoud-ALI',
            'slug'=>''
        ]);
        $dbpost=Post::Q('ali')->first();
        $this->assertNotNull($dbpost);
        $this->assertTrue($dbpost->id==$post->id);
    }
    public function testslug()
    {
        $post=factory(Post::class)->create([
            'slug'=>'mfathi',
            'title'=>''

        ]);
        $dbpost=Post::Q('fathi')->first();
        $this->assertNotNull($dbpost);
        $this->assertTrue($dbpost->id==$post->id);

    }
    public function testslugNull()
    {
        $post=factory(Post::class)->create([
            'slug'=>'m1',
            'title'=>''

        ]);
        $dbpost=Post::Q('fathi')->first();
        $this->assertNull($dbpost);
    }
    //------------------------------test hasvideo-------------------------------------
    public function testvideo()
    {
        $post=factory(Post::class)->create([
            'video_url'=>'https://www.youtube.com/jjj',
        ]);
        $dbpost=Post::Hasvideo()->first();
        $this->assertNotNull($dbpost);
        $this->assertTrue($dbpost->id==$post->id);
    }
    public function testnotvideo()
    {
        $post=factory(Post::class)->create([
            'video_url'=>'https://www.youtube.com/jjj',


        ]);
        $dbpost=Post::Hasnotvideo()->first();
        $this->assertNull($dbpost);
//        $this->assertTrue($dbpost->id==$post->id);
    }
}
