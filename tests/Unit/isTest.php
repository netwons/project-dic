<?php

namespace Tests\Unit;

use Tests\TestCase;

class isTest extends TestCase
{

    public function testone()
    {
        $response=\App\Http\Controllers\Controller::is_id_term('id:1');
        $this->assertTrue($response);
    }
    public function teststring()
    {
        $response=\App\Http\Controllers\Controller::is_id_term('id:louc');
        $this->assertFalse($response);
    }
    public function testid()
    {
        $response=\App\Http\Controllers\Controller::is_id_term('id:123',$id);
        $this->assertTrue($response);
//        dd($id);
        $this->assertTrue($id==123);
    }
    public function testcustomename()
    {
        $response=\App\Http\Controllers\Controller::is_id_term('user_id:777',$id,'user_id');
        $this->assertTrue($response);
        $this->assertTrue($id==777);
    }
}
