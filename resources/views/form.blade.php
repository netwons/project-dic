
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Form</h1>

    <form action="{{route('form1')}}" method="post">
        {{csrf_field()}}
        <input type="text" name="name" value="{{old('name')}}"><br>
        <input type="text" name="email" value="{{old('name')}}"><br>
        <input type="text" name="password"><br>
        <div>
            <input type="submit" value="submit"><br>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </form>
</body>
</html>
