<?php


namespace App\repository\userrepository;


use App\DicModel;
use App\Facades\facadelaravel;
use App\Facades\userrepositoryfacade;
use App\User;
use \Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class eloquentuserrepository implements userrepositoryinterface
{
    public function all()
    {
        return DicModel::where('id', '<', '50')->orderBy('id', 'desc')->take('20')->get();
    }

    public function all1()
    {
        return DicModel::where('id', '<', '150')->orderBy('id', 'desc')->take('20')->get();
    }

    public function find($id)
    {
        return DicModel::find($id);
    }

    public function edit($id)
    {
        return userrepositoryfacade::valid_edit($id);
    }


    public function form(array $post)
    {
        return User::create([
            'name' => $post['name'],
            'email' => $post['email'],
            'password' => bcrypt($post['password']),
        ]);
    }

    public function destroy($id)
    {
        return userrepositoryfacade::valid_destroy($id);
    }

    public function cache_redis()
    {

        if (! is_null(Redis::get('articles'))) {
            return unserialize(Redis::get('articles'));
        }
        $article = User::all();
        return Redis::setex('articles', 600, serialize($article));


    }
}
