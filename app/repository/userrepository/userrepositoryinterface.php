<?php

namespace App\repository\userrepository;

interface userrepositoryinterface
{
    public function all();

    public function all1();

    public function find($id);

    public function edit($id);

    public function form(array $post);

    public function destroy($id);

    public function cache_redis();

}
