<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table='posts';
    protected $fillable=['title','slug','body','video_url','published_at'];
    protected $dates=['published_at'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at')
            ->whereDate('published_at','<=',today());
    }

    /**
     * @param $query
     * @param string $q
     * @return mixed
     */
    public function scopeQ($query,string $q)
    {
        return $query->where(function (Builder $query)use ($q){
           $term="%$q%";
           return $query->orWhere('title','like',$term)
               ->orWhere('slug','like',$term);
        });
    }

    /**
     * @param $query
     * @param bool $has
     * @return mixed
     */
    public function scopeHasvideo($query,bool $has=true)
    {
        if($has)
        {
            return $query->whereNotNull('video_url');

        }else{
            return $query->whereNull('video_url');
        }
    }

    public function scopeHasnotvideo($query)
    {
        return $query->hasVideo(false);
    }


}
