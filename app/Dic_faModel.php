<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dic_faModel extends Model
{
    protected $table="dic_fa_models";
    protected $fillable=["name","name_fa","synonym","Sentence"];
}
