<?php

namespace App\Providers;

use App\Facades\facadelaravel;
use App\Facades\userrepositoryfacade;
use App\repository\userrepository\elasticuserrepository;
use App\repository\userrepository\eloquentuserrepository;
use App\repository\userrepository\userrepositoryinterface;
use App\repository\userrepository\validation;
use App\uelasticrepository\elasticdbrepository;
use App\uelasticrepository\elasticdbrepositoryinterface;
use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class Repositoryserviceprovider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        userrepositoryfacade::shouldProxyTo(validation::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->bind(userrepositoryinterface::class, eloquentuserrepository::class);
        $this->app->bind(elasticdbrepositoryinterface::class, elasticdbrepository::class);
//        $this->app->bind(userrepositoryinterface::class,elasticuserrepository::class);
    }
}
