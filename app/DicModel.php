<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DicModel extends Model
{
    protected $table="dic_models";
    protected $fillable=["name","name_fa","synonym","Sentence","path_pronunciation","pronunciation","meaning"
        ,"sentence_similar","sentence_similar2","specialty"
    ];
}
