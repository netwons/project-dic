<?php

namespace App\Http\Controllers;

use App\Dic_faModel;
use App\DicModel;
use App\Jobs\sendmail;
use App\Jobs\sendwelcome;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function PHPUnit\Framework\isJson;
use function PHPUnit\Framework\matches;

class DicController extends Controller
{
    public function robot_word()
    {
        $i = 1829;
        while ($i < 1874){
            set_time_limit(0);
            sleep(5);
            $client = new \GuzzleHttp\Client();
            $url = "https://abadis.ir/entoen?pagenumber=" . $i;
            $res = $client->request('GET', $url,[
                'proxy'=>[
                    // 'https'=>'127.0.0.1:35717',
                    'http'=>'176.9.119.170:3128'
                ]
            ]);
//            return $res->getBody();
            $result=$res->getBody();
            //TODO گرفتن کلمه ها
            preg_match_all("/<div class=\"BoxBody BoxJustify NoMore BoxLinks\"(.+?)*>[\W\w]*div class='Paging/im",$result,$match_word);
            $impload=implode(",",$match_word[0]);
            $file=fopen("words.txt",'w');
            fwrite($file,$impload);
            fclose($file);
            $readfile=file_get_contents("words.txt") or  die("words.txt not found");
            preg_match_all("/>(.*?)*<\/a>/im",$readfile,$match_word1);
            $impload1=implode("\n",$match_word1[0]);
            $str=str_replace("</a>","",$impload1);
            $str1=str_replace(">","",$str);
            $file1=fopen("words1.txt",'w');
            fwrite($file1,$str1);
            fclose($file1);
            //TODO پایان
            //============================================================================
            //TODO save to database
            $file2 = fopen("words1.txt", 'r');
            while (!feof($file2)) {
                $word_asle=fgets($file2);
                $user=DicModel::firstOrCreate(
                    [
                     'name'=>$word_asle,
                    ],[
                        'name'=>$word_asle
                    ]
                );
            }
            //TODO end database
            echo $i."<br>";
            $i++;
       }
    }
//==================================================================
    public function robot_word_fa()
    {
        $i=3242;
        while ($i < 3243) {
            set_time_limit(0);
            sleep(5);
            $client = new \GuzzleHttp\Client();
            $url = "https://abadis.ir/fatofa?pagenumber=" . $i;
//            $res = $client->request('GET', $url);
            $res = $client->request('GET', $url, [
                'proxy' => [
//                    'https' => '127.0.0.1:35717',
//                    'https' => '139.255.11.150:8080',
                    'http' => '182.52.90.117:45535',
                ]
            ]);
//            return $res->getBody();
            $result = $res->getBody();
            //TODO گرفتن کلمه ها
            preg_match_all("/<div class=\"BoxBody BoxJustify NoMore BoxLinks\"(.+?)*>[\W\w]*div class='Paging/im",$result,$match_word);
//            return $match_word[0];
            $impload=implode(",",$match_word[0]);
//            return $impload;
            $file=fopen("words_fa.txt",'w');
            fwrite($file,$impload);
            fclose($file);
            $readfile=file_get_contents("words_fa.txt") or  die("words_fa.txt not found");
            preg_match_all("/>(.*?)*<\/a>/im",$readfile,$match_word1);
            $impload1=implode("\n",$match_word1[0]);
            $str=str_replace("</a>","",$impload1);
            $str1=str_replace(">","",$str);
            $file1=fopen("words1_fa.txt",'w');
            fwrite($file1,$str1);
            fclose($file1);

            //TODO پایان
            //============================================================================
            //TODO save to database
            $file2 = fopen("words1_fa.txt", 'r');
            while (!feof($file2)) {
                $word_asle=fgets($file2);
                $user=Dic_faModel::firstOrCreate(
                    [
                        'name_fa'=>$word_asle,
                    ],[
                        'name_fa'=>$word_asle
                    ]
                );
            }
            //TODO end database
            echo $i."<br>";
            $i++;
        }
    }
    //===========================================================
    public function unicode_decode(string $str)
    {
        return preg_replace_callback('/u([0-9a-f]{4})/i', function ($match) {
            return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
        }, $str);
    }
//==================================================================
    public function robot_details()
    {
        $p=9650;
        $b=0;
        $f=0;
        while($p<136275){

            $id_name=DicModel::where('id',$p)->value('name');
            $t=str_replace("\n","",$id_name);
//            dd($t);
            $first_character=mb_substr($id_name, 0, 1, 'utf-8'); //TODO اولین کاراکتر رو بر میگردونه
            set_time_limit(0);
            sleep(6);
            $file_proxy=file('http.txt');
            $count_proxy=count($file_proxy);
            $ij=rand(0,2);
            $i_cookie=rand(0,1);
            $arry=array(
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36");
            $arry_cookie=array("_ga=GA1.2.1663636340.1608118159; analytics_token=8be65005-4351-4bbd-8abc-397c6ac1d315; _yngt=9845ccfc-330b-4907-c913-03eec9ffee30; analytics_campaign={%22source%22:%22google%22%2C%22medium%22:%22organic%22}; TransItem=trfaen; _gid=GA1.2.168988178.1609397299; DicItem=entofa,entoen,abbr; ASP.NET_SessionId=gherbmxddvooai4c0yqmn2tb; _5e4d5be26e25f32c421e21a4=true; MEDIAAD_USER_ID=29ba9859-6c74-4a10-a232-7ff26f7fd135; analytics_session_token=b32f2ad4-f8db-7b3d-6228-5541e565f84f; yektanet_session_last_activity=1/2/2021; _yngt_iframe=1; content-view-yn-notification-2780=101; _gat=1",
    "DicItem=entofa,entoen,abbr");
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, "https://abadis.ir/?lntype=entofa,entoen,abbr&word=".$t."&from=ac");
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
////            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
////            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
//            curl_setopt($ch, CURLOPT_PROXY, "89.187.177.85:80");
//            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
////            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
////            curl_setopt($ch, CURLOPT_COOKIEJAR,"rs.txt");
////            curl_setopt($ch, CURLOPT_DNS_USE_GLOBAL_CACHE, false );
////            curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 2 );
//
//            $headers = array();
//            $headers[] = 'User-Agent: '.$arry[$ij];
//            $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
//            $headers[] = 'Accept-Language: en-US,en;q=0.5';
//            $headers[] = 'Connection: keep-alive';
//            $headers[] = 'Referer: https://abadis.ir/';
//            $headers[] = 'Cookie: '.$arry_cookie[$i_cookie];
//            $headers[] = 'Upgrade-Insecure-Requests: 1';
//            $headers[] = 'Te: Trailers';
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//
//            $result = curl_exec($ch);
//            if (curl_errno($ch)) {
//                echo 'Error:' . curl_error($ch);
//
//            }
//            curl_close($ch);
////
//            return $result;
            $client = new \GuzzleHttp\Client();
            $url="https://abadis.ir/?lntype=entofa,entoen,abbr&word=".$t."&from=ac";
//            $url="https://abadis.ir/b/".$t;
//            $url = "https://abadis.ir/entofa/{$first_character}/".$t;
             FOR1:


            $res=$client->request('GET', $url,[
                "proxy" => "http://hkedduey-dest:7ui4c71wylgn@".trim($file_proxy[$b]),
                'timeout'=>0,
//               'debug' => true,
                // 'headers' => [
                //     'User-Agent' =>$arry[$ij],
                //     'Cookie' =>$arry_cookie[$i_cookie],
                //     'Te'=>'Trailers',
                //     'Upgrade-Insecure-Requests'=>1,
                //     'Referer'=>'https://abadis.ir/',
                //     'Connection'=>'keep-alive',
                //     'Accept-Language'=>'en-US,en;q=0.5',
                //     'Accept'=>'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                // ]
            ]);

            $result=$res->getBody();
//           return $result;
            $result1=$res->getStatusCode();
            // return $result1;
            if($result1!=200){
                $b++;
                 goto FOR1;
                }
            preg_match_all("/HTTP Error 400./im", $result, $matches_error);

            $t_error=implode("\n",$matches_error[0]);
            if($t_error=="HTTP Error 400."){
                break;
            }
            $file=fopen("info.txt",'w');
            fwrite($file,$result);
            fclose($file);
            $readfile=file_get_contents("info.txt") or  die("info.txt not found");
            //TODO تلفظ
            preg_match_all("/(?<=<\/a>)\/[\W\w]*?\//im",$readfile,$matchs);
            $t=implode("\n",$matchs[0]);
            $file0=fopen("info_t.txt",'w');
            fwrite($file0,$t);
            fclose($file0);
            $readfile0=fopen("info_t.txt",'r') or  die("info_t.txt not found");
            $name_t=[];
            while (!feof($readfile0)) {
                $word_asle = fgets($readfile0);
//                echo $word_asle;
                array_push($name_t,$word_asle);
            }
//            return $name_t;
            $r_t = json_encode($name_t);
//            return $r_t;
            $r_t=$this->unicode_decode($r_t);
            $r_t=str_replace("\\","",$r_t);
            $r_t=str_replace("n","",$r_t);
//                return $r_t;//result similar
            if(file_exists("info_t.txt")){
                unlink("info_t.txt");
            }
            //TODO پایان تلفظ

            //TODO معنی
            preg_match_all("/(?<=\/table><hr>)[\W\w]*?<\/div/im",$readfile,$matchs1);
            $impload=implode(",",$matchs1[0]);
//            return $matchs1[0];
            $str=str_replace("</div","",$impload);
//            return $str;
            //TODO پایان معنی

            //TODO جملات مشابه
//            preg_match_all("/MCP_lbl_Term[\W\w]*?<\/span/im",$readfile,$matchssim);
            preg_match_all("/<label>جمله های نمونه<\/label>[\W\w]*?<\/div><\/div/im",$readfile,$matchssim);
            $implod_similar=implode(",",$matchssim[0]);
//            return $implod_similar;
            if(!empty($implod_similar)) {
                $file2 = fopen("infosimilar.txt", 'w');
                fwrite($file2, $implod_similar);
                fclose($file2);
                $readfile1 = file_get_contents("infosimilar.txt") or die("infosimilar.txt not found");
                preg_match_all("/(?<=AnaTermExp\'>)[0-9]{1,2}[\W\w]*?<\/div/im", $readfile1, $matchsimilar);
                preg_match_all("/(?<=AnaTermMean\' TermID=\')[0-9]{1,2}[\W\w]*?<\/div/im", $readfile1, $matchsimilar1);
//                return $matchsimilar1[0];
                $matchsimilar = preg_replace("/[0-9]{1,2}\. /im", "", $matchsimilar[0]);//حذف پرامتر های اضافه
                $matchsimilar1 = preg_replace("/[0-9]{1,2}\' Word=\'(.+?)\'>/im", "", $matchsimilar1[0]);//حذف پرامتر های اضافه
                $matchsimilar = preg_replace("/<\/div/im", "", $matchsimilar);//حذف پرامتر های اضافه
                $matchsimilar1 = preg_replace("/<\/div/im", "", $matchsimilar1);//حذف پرامتر های اضافه
                $count = count($matchsimilar);
                $count_1 = count($matchsimilar1);
//            return $matchsimilar;
//                return $count;
//                return $matchsimilar1;

                $j = 0;
                $name = [];
                //TODO به صورت json در آوردیم
                if($count_1 !=0 And $count !=0) {
                    if($count>$count_1) {
                        while ($j < $count-1) {
                            $name[$matchsimilar[$j]] = $matchsimilar1[$j];
//                            var_dump($name);
//                            exit();
                            $j++;
                        }
                        $r_simi = json_encode($name);
                        $r_simi = $this->unicode_decode($r_simi);
                        $r_simi = str_replace("\\", "", $r_simi);
//                        return $r_simi;
                        DicModel::where('id', $p)->update(['sentence_similar' => $r_simi]);
                    }elseif($count==$count_1){
                        while ($j < $count) {
                            $name[$matchsimilar[$j]] = $matchsimilar1[$j];

                            $j++;
                        }
                        $r_simi = json_encode($name);
                        $r_simi = $this->unicode_decode($r_simi);
                        $r_simi = str_replace("\\", "", $r_simi);
                        DicModel::where('id', $p)->update(['sentence_similar' => $r_simi]);
                    }
                }
            }
//            return $r_simi;

            //TODO پایان
            if(file_exists("infosimilar.txt")){
                unlink("infosimilar.txt");
            }
            //TODO پایان

            //TODO مشابه جدید
            preg_match_all("/MCP_lbl_Fa[\W\w]*?<\/b><\/span>/im",$readfile,$match_sim);
            $sim_implod=implode(",",$match_sim[0]);
            if(!empty($sim_implod)) {
                $file3 = fopen("infosimilar1.txt", 'w');
                fwrite($file3, $sim_implod);
                fclose($file3);
                $readfile2 = file_get_contents("infosimilar1.txt") or die("infosimilar1.txt not found");
                preg_match_all("/<a href[\W\w]*?<\/a>/im", $readfile2, $match_sim_one);
                preg_match_all("/<div class=\'M[\W\w]*?<\/div>/im", $readfile2, $match_sim_two);
                $match_sim_one = preg_replace("/<a href[\W\w]*?\">/im", "", $match_sim_one[0]);//حذف پرامتر های اضافه
                $match_sim_one = preg_replace("/<\/a>/im", "", $match_sim_one);//حذف پرامتر های اضافه
                $match_sim_two = preg_replace("/<div[\W\w]*?\'>/im", "", $match_sim_two[0]);//حذف پرامتر های اضافه
                $match_sim_two = preg_replace("/<\/div>/im", "", $match_sim_two);//حذف پرامتر های اضافه
                $count_sim=count($match_sim_one);
                $count_sim_two=count($match_sim_two);

                //return $count_sim;
//            dump($match_sim_one);
//            dump($match_sim_two);exit();
                //TODO به صورت json در آوردیم
                $k = 0;
                $name_sim = [];
                if($count_sim==$count_sim_two){
                    while ($k < $count_sim) {
                        $name_sim[$match_sim_one[$k]] = $match_sim_two[$k];
                        $k++;
                    }
                    $r_sim = json_encode($name_sim);
                    $r_sim=$this->unicode_decode($r_sim);
                    $r_sim=str_replace("\\","",$r_sim);
//                  return $r_sim;//result similar
                }elseif($count_sim>$count_sim_two){
                    while ($k < $count_sim_two) {
                        $name_sim[$match_sim_one[$k]] = $match_sim_two[$k];
                        $k++;
                    }
                    $r_sim = json_encode($name_sim);
                    $r_sim=$this->unicode_decode($r_sim);
                    $r_sim=str_replace("\\","",$r_sim);
//                  return $r_sim;//result similar

                }else{
                    while ($k < $count_sim) {
                        $name_sim[$match_sim_one[$k]] = $match_sim_two[$k];
                        $k++;
                    }
                    $r_sim = json_encode($name_sim);
                    $r_sim=$this->unicode_decode($r_sim);
                    $r_sim=str_replace("\\","",$r_sim);
//                  return $r_sim;//result similar
                }
                DicModel::where('id',$p)->update(['sentence_similar2'=>$r_sim]);
                if (file_exists("infosimilar1.txt")) {
                    unlink("infosimilar1.txt");
                }
            }
            //TODO پایان

            //TODO تخصصی
//            preg_match_all("/MCP_lbl_Expert[\W\w]*?<\/span>/im",$readfile,$match_Specialty);
            preg_match_all("/<label>تخصصی<\/label>[\W\w]*?<\/div><\/div/im",$readfile,$match_Specialty);
            $Specialty_implod=implode(",",$match_Specialty[0]);
//            return $Specialty_implod;
            if(!empty($Specialty_implod)) {
                $file4 = fopen("infoSpecialty.txt", 'w');
                fwrite($file4, $Specialty_implod);
                fclose($file4);
                $readfile4 = file_get_contents("infoSpecialty.txt") or die("infoSpecialty.txt not found");
//                preg_match_all("/<\/div>[\W\w]*?<b class/im", $readfile4, $matchSpecialt);
//                $str_p = str_replace("</div>", "", $matchSpecialt[0]);
//                $str_p1 = str_replace("<b class", "", $str_p);
//                $str_p2 = implode(",", $str_p1);
                //TODO جدید
                preg_match_all("/(?<=<div class=\'WordA\'>)[\W\w]*?<\/div>/im",$readfile4,$matchSpecialt);
                $match_spec = preg_replace("/<\/div>/im", "", $matchSpecialt[0]);//حذف پرامتر های اضافه
                $count_spec=count($match_spec);
//                return $match_spec;
                preg_match_all("/(?<=<div class=\'Mean\'>)[\W\w]*?<\/div/im",$readfile4,$matchspec_m);
                $match_spec_m = preg_replace("/<\/div/im", "", $matchspec_m[0]);//حذف پرامتر های اضافه
                $count_spec_as=count($match_spec_m);
//                return $match_spec_m;
//                return $count_spec_as;
                $s1 = 0;
                $name_sp=[];
                //TODO به صورت json در آوردیم
                if($count_spec==$count_spec_as) {
                    while ($s1 < $count_spec) {
                        $name_sp[$match_spec[$s1]] = $match_spec_m[$s1];

                        $s1++;
                    }
                    $r_sp = json_encode($name_sp);
                    $r_sp = $this->unicode_decode($r_sp);//result synon
                    $r_sp = str_replace("\\", "", $r_sp);
                }elseif($count_spec>$count_spec_as){
                    while ($s1 < $count_spec-1) {
                        $name_sp[$match_spec[$s1]] = $match_spec_m[$s1];

                        $s1++;
                    }
                    $r_sp = json_encode($name_sp);
                    $r_sp = $this->unicode_decode($r_sp);//result synon
                    $r_sp = str_replace("\\", "", $r_sp);
                }else{
                    while ($s1 < $count_spec_as-1) {
                        $name_sp[$match_spec[$s1]] = $match_spec_m[$s1];

                        $s1++;
                    }
                    $r_sp = json_encode($name_sp);
                    $r_sp = $this->unicode_decode($r_sp);//result synon
                    $r_sp = str_replace("\\", "", $r_sp);
                }
//                return $r_sp;
                //TODO پایان جدید
                if (file_exists("infoSpecialty.txt")) {
                    unlink("infoSpecialty.txt");
                }
                DicModel::where('id',$p)->update(['specialty'=>$r_sp]);

            }
            //TODO پایان

            //TODO انگلیسی به انگلیسی
//            preg_match_all("/<div class=\"Lun\" title=\"نگلیسی به انگلیسی\"[\W\w]*?<\/span/im",$readfile,$match_english);
            preg_match_all("/(?<=<label>انگلیسی به انگلیسی<\/label>)[\W\w]*?<\/div><\/div/im",$readfile,$match_english);
            $english_implod=implode(",",$match_english[0]);
//            return $english_implod;
            if(!empty($english_implod)) {
                $file5 = fopen("infoenglish.txt", 'w');
                fwrite($file5, $english_implod);
                fclose($file5);
                $readfile5 = file_get_contents("infoenglish.txt") or die("infoenglish.txt not found");
                preg_match("/<div class=\'Mean[\W\w]*?<\/div/i", $readfile5, $matchenglish);
                $match_en = preg_replace("/<div class=\'Mean\'>/im", "", $matchenglish);//حذف پرامتر های اضافه
                $match_en1 = preg_replace("/<\/div/im", "", $match_en);//حذف پرامتر های اضافه
                $match_en1=implode(",",$match_en1);
//                return $match_en1;
                DicModel::where('id',$p)->update(['Sentence'=>$match_en1]);

//            return $match_en;//english to english
                if (file_exists("infoenglish.txt")) {
                    unlink("infoenglish.txt");
                }
            }
            //TODO پایان

            //TODO مترادف
//            preg_match_all("/<div class=\"Lun\" title=\"مترادف\">[\W\w]*?<\/div><\/span>/im",$readfile,$match_Synonyms);
            preg_match_all("/<label>مترادف<\/label>[\W\w]*?<\/div><\/div/im",$readfile,$match_Synonymss);
            $Synonyms_implod=implode(",",$match_Synonymss[0]);
//            return $Synonyms_implod;
            if(!empty($Synonyms_implod)) {
                $file6 = fopen("infosynonyms.txt", 'w');
                fwrite($file6, $Synonyms_implod);
                fclose($file6);
                $readfile6 = file_get_contents("infosynonyms.txt") or die("infosynonyms.txt not found");

                preg_match_all("/(?<=<div class=\'WordB\'>)[\W\w]*?<span /im",$readfile6,$matchsynonyms);
                $match_synon = preg_replace("/ <span /im", "", $matchsynonyms[0]);//حذف پرامتر های اضافه
                $count_synon=count($match_synon);

                preg_match_all("/(?<=<span class=\'MeanType\'>)[\W\w]*?<\/span/im",$readfile6,$matchsynonyms_as);
                $match_synon_as = preg_replace("/<\/span/im", "", $matchsynonyms_as[0]);//حذف پرامتر های اضافه
//                $count_synon_as=count($match_synon_as);

                preg_match_all("/(?<=<div class=\'Mean\'>)[\W\w]*?<\/div>/im",$readfile6,$matchsynonyms_similar);
                $match_synon_similar = preg_replace("/<\/div>/im", "", $matchsynonyms_similar[0]);//حذف پرامتر های اضافه
//                $count_synon_similar=count($match_synon_similar);
//                    return $match_synon_similar;
//                return $match_synon;
//                return $match_synon_as;
//                return $match_synon_similar;
                $s = 0;
                //TODO به صورت json در آوردیم
                while ($s < $count_synon) {
                    $f=[];
                    $f=$match_synon[$s].$match_synon_as[$s];
                    $name_synon[$f] = $match_synon_similar[$s];
                    $s++;
                }
                $r_synon = json_encode($name_synon);
//                return $r_synon;//result synon
                DicModel::where('id',$p)->update(['synonym'=>$r_synon]);
                if (file_exists("infosynonyms.txt")) {
                    unlink("infosynonyms.txt");
                }
            }
            //TODO پایان



            $user=DicModel::where('id',$p)->update([
                'name_fa'=>$str,
                'pronunciation'=>$r_t,
            ]);

//            echo $p."=>".$file_proxy[$b].'==>'.$b."ip:".$_SERVER['REMOTE_ADDR']."<br>";
           if($b<$count_proxy-1){
               $b++;
           }else{
               $b=0;
               $f++;
               sleep(1);
               if($f==29){
                break;
               }
           }
//             if($p==2170){
//                 break;
//             }else {
//                exit();
                $p++;
//              }

        }

    }


    public function path_pronunciation($word)
    {
        $i=0;
        $arr=array('us','uk','IN');
        while($i<3) {

            $url = "https://translate.google.com/translate_tts?ie=UTF-8&client=gtx&q=" . trim($word) . "&tl=en-".$arr[0];
            $t=storage_path('app/public/');
            $date=date('Y').'/'.date('m').'/'.date('d').'/'.$word;
            if(! is_dir($date)){
                Storage::disk('public')->makeDirectory($date);
            }
//            $path=Storage::disk('local')->url($date);

            $path=$t.$date."/".$word.'_'.$arr[$i].'.mp3';
//            return $path;
            $file = fopen($path, "w+");
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_FILE, $file);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 300);
            curl_setopt($ch, CURLOPT_USERAGENT, 'mozilla/5.0');
            $re = curl_exec($ch);
            curl_close($ch);
            fclose($file);
            $i++;
        }
    }

    public function job()
    {

        $user=User::create([
           'name'=> "alireza14",
            'email'=>'net17@gmail.com',
            'password'=>bcrypt("12344")
        ]);

        sendmail::dispatch($user)->onQueue("email")->delay(now()->addSeconds(2));
//        sendwelcome::dispatch()->onQueue('error');
        return response($user,400);
        exit();
        sendwelcome::withChain([
           new sendmail(),
           new sendmail(),
        ])->dispatch()->delay(now()->addSeconds(2));

        $job=new \App\Jobs\sendmail();
        $job=$job->onQueue('default');
        echo $this->dispatch($job);
    }
}
