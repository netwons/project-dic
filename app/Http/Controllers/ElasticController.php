<?php

namespace App\Http\Controllers;

use App\uelasticrepository\elasticdbrepositoryinterface;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;

class ElasticController extends Controller
{
    protected $elasticrepository;
    protected $elasticsearch;

    public function __construct(elasticdbrepositoryinterface $elasticrepository)
    {
        $this->elasticrepository=$elasticrepository;
        $this->elasticsearch=ClientBuilder::create()->build();
    }

    public function all()
    {
        //dd($this->elasticsearch);
        return $this->elasticrepository->all();
    }
}
