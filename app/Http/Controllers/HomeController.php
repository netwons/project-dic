<?php

namespace App\Http\Controllers;

use App\DicModel;
use App\Http\Requests\UserRequest;
use App\repository\userrepository\eloquentuserrepository;
use App\repository\userrepository\userrepositoryinterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    protected $userrepository;
    public function __construct(userrepositoryinterface $userrepository)
    {

        $this->userrepository=$userrepository;
    }

    public function index()
    {
        return $this->userrepository->all();
    }
    public function index1()
    {
        return $this->userrepository->all1();
    }

    public function find($id)
    {
        return $this->userrepository->find($id);
    }

    public function edit($id)
    {
        return $this->userrepository->edit($id);
    }

    public function form(UserRequest $request)
    {
        return $this->userrepository->form($request->all());
    }

    public function destroy($id)
    {
        return $this->userrepository->destroy($id);
    }

    public function cache_redis()
    {
        return $this->userrepository->cache_redis();
    }
}
